# Modelling the effect of a border closure between Switzerland and Italy on the spatiotemporal spread of COVID-19 in Switzerland 

This repository contains the data and code used to perform the analysis of the article: 

* M. Grimée, M. Bekker-Nielsen Dunbar, F. Hofmann, L. Held. Modelling the effect of a border closure between Switzerland and Italy on the spatiotemporal spread of COVID-19 in Switzerland. *Spat. Stat.*, 2021. DOI: [10.1016/j.spasta.2021.100552](https://doi.org/10.1016/j.spasta.2021.100552)

### The project
This article was produced within the framework of the SUSPend project, which was supported by the Swiss National Science Foundation [grant number 196247, https://data.snf.ch/covid-19/snsf/196247].


### This repository contains
* `Data_raw` : contains all the raw data as .csv or .txt files. Descriptions of data sources can be found in the article's supplementary materials.
* `Data_preparation`: contains R-code for the data preprocessing and the resulting R-objects 
* `Analysis`: contains R-code for the analysis and the resulting R-objects
* `Figures`: contains figures as .pdf files created under `Analysis/Plot_scripts`
* `Tables`: contains results tables as .tex files created under `Analysis`
* All objects with extension `_sens` pertain to the sensitivity analysis around the IOM indicator. 

For each script, a short description of its purpose is provided at the beginning of the file.


### How to use the code
* The files in the directories `Data_preparation`, `Analysis` and `Tables` can be built by running the makefile, with exception of the objects with file name ending in `_sens` (see next point).

* To build all objects with file name ending in `_sens`, the R-scripts `Data_preparation/Adjacencies_prep_iom_sensitivity.R`, then `Data_preparation/Adjacencies_cf_sens.R`, and finally `Analysis/Sensitivity.R` have to be ran separately.

* To build the files under `Figures`, the R-scripts under `Analysis/Plot_scripts` have to be ran separately.

