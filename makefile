# group R objects created in one single script together
#set_parameters
parameters = Data_preparation/parameters.Rdata
#FB_prep
facebook = Data_preparation/fb_mov.rds
#IOM_prep
iom = Data_preparation/iom.rds
#Adjacencies_prep
adjacencies = Data_preparation/adjacencies_BASE.rds	Data_preparation/adjacencies_FB.rds	Data_preparation/adjacencies_IOM.rds
#cases_prep
cases = Data_preparation/cases_per_ctry.Rdata Data_preparation/cases.rds
#population
population = Data_preparation/pop.rds
#density
density = Data_preparation/density.rds
#population of biggest city per region
density_city = Data_preparation/density_city.rds
#trate_prep
trate = Data_preparation/trate.rds
#Temperature_prep
temperature = Data_preparation/temp.rds
#covariates
covariates = Data_preparation/covariates.Rdata
#analysis
models = Analysis/overview.Rdata Analysis/inputs.Rdata Analysis/models.Rdata Tables/models.tex Tables/best1.tex Tables/best1descr.tex Tables/best2.tex Tables/best2descr.tex Tables/best3.tex Tables/best3descr.tex
#counterfactual matrices
cfmatrix = Data_preparation/Adjmat_cf.Rdata
#prediction
#prediction = Tables/results.tex Analysis/results.rds Analysis/mom_pred.Rdata
#uncertainty imputation
uncertainty = Analysis/adjmat_list.Rdata Analysis/coef_list.rds Analysis/adjmat_list_A.rds Analysis/adjmat_list_B.rds Analysis/predictions.rds Analysis/mom.Rdata daily_predictions.Rdata Analysis/results_uncertainty.rds Tables/results_uncertainty.tex

all_vars: $(parameters) $(facebook) $(iom) $(adjacencies) $(cfmatrix) $(population) $(density) $(density_city) $(trate) $(temperature) $(covariates) $(cases) $(models) $(uncertainty)


#define rules
#step 1: set parameters such as start and end date, countries considered, etc.
$(parameters):
	cd Data_preparation ; Rscript set_parameters.R

#step 2: prepare facebook data
$(facebook): $(parameters)
	cd Data_preparation ; Rscript Facebook_prep.R

#step 3: prepare IOM data
$(iom): $(parameters)
	cd Data_preparation ; Rscript IOM_prep.R

$(adjacencies): $(parameters) $(facebook) $(iom)
	cd Data_preparation ; Rscript Adjacencies_prep.R

$(cfmatrix): $(parameters) $(adjacencies)
	cd Data_preparation ; Rscript Adjacencies_cf.R

$(cases): $(parameters)
	cd Data_preparation ; Rscript Cases_prep.R

#step 6: prepare covariates
$(population): $(parameters)
	cd Data_preparation ; Rscript Population.R

$(density): $(parameters)
	cd Data_preparation ; Rscript Density_prep.R

$(density_city): $(parameters)
	cd Data_preparation ; Rscript Density_city_prep.R

$(trate): $(population) $(parameters)
	cd Data_preparation ; Rscript Trate_prep.R

$(temperature): $(parameters)
	cd Data_preparation ; Rscript Temperature_prep.R

#population, testing rates, temperature, weekday, ctry indicator,
$(covariates): $(parameters) $(temperature) $(density) $(density_city)
	cd Data_preparation ; Rscript Covariates_prep.R

#analysis
$(models): $(parameters) $(covariates) $(cases) $(population) $(adjacencies)
	cd Analysis ; Rscript Analyses_CH_IT.R

$(uncertainty): $(parameters) $(facebook) $(iom) $(covariates) $(cases) $(population) $(adjacencies)
	cd Analysis ; Rscript Imputation_decay.R
