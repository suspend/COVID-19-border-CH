####################
## Set parameters ##
##  for analyses  ##
####################
#In this script, we set parameters that return frequently in the analysis,
#such as the NUTS-codes for the countries and regions considered,
#and the time-frame
#we save the parameterd into an Rdata file.


countries <- c("ITA", "CHE") # alpha-III
countries_II <- c("IT", "CH") # alpha-II
rorder <- c("CH01", "CH02", "CH03", "CH04", "CH05","CH06", "CH07",
            "ITC1", "ITC2", "ITC4", "ITH1")
ncol <- length(rorder)
start_date <- as.Date("2020-02-24")
end_date <- as.Date("2020-08-04")
sts_dates <- seq(start_date, end_date, "day")
nrow <- length(sts_dates)

save(countries, countries_II, rorder, start_date, end_date, sts_dates,
     nrow, ncol,
     file = "parameters.Rdata")
